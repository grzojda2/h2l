<?php

namespace H2l;

use Illuminate\Database\Eloquent\Model;
use H2l\Champion;
class TeamMatch extends Model
{
	protected $fillable = ['match_id', 'team_id', 'kills', 'deaths', 'assists', 'cs','first_blood', 'first_baron','first_tower','first_dragon','win', 'herald_kill','baron_kills','tower_kills','dragon_kills','inhibitor_kills'];
    public static function makeStatistics($games,$leagueId)
    {
    	foreach($games as $game)
    	{
    		$kills   = 0;
    		$assists = 0;
    		$deaths  = 0;
    		$cs 	 = 0;
    		foreach($game['participantIdentities'] as $pi)
    		{
    			if($pi['player']['accountId']==$leagueId)
    			{
    				$participantId = $pi['participantId'];
    				$team 		   = $game['participants'][$participantId-1]['teamId'];
    			}
    		}
    		if(!TeamMatch::isTeam($game['gameId'],$team))
    		{
    			foreach($game['teams'] as $teamArray)
    			{
    				if($teamArray['teamId']==$team)
    				{
    					foreach($teamArray['bans'] as $ban)
    					{
    						Champion::addBaned($ban['championId']);
    					}
    				}
    			}
	    		foreach($game['participants'] as $player)
	    		{
	    			$playerStats = $player['stats'];
	    			Champion::addPicked($player['championId']);
	    			if($player['teamId']==$team)
	    			{
	    				$kills 	 += $playerStats['kills'];
	    				$assists += $playerStats['assists'];
	    				$deaths  += $playerStats['deaths'];
	    				$cs 	 += $playerStats['totalMinionsKilled'];
	    			}
	    		}
	    		$teamStats = ($team == 100 ? $game['teams'][0] : $game['teams'][1]);
	    			TeamMatch::Create([
	    			'match_id' 		  => $game['gameId'],
	    			'team_id' 		  => $team,
	    			'kills' 		  => $kills,
	    			'deaths' 		  => $deaths,
	    			'assists' 		  => $assists,
	    			'cs' 			  => $cs,
	    			'first_blood' 	  => ($teamStats['firstBlood'] ? 1 : 0),
	    			'first_baron' 	  => ($teamStats['firstBaron'] ? 1 : 0),
	    			'first_tower' 	  => ($teamStats['firstTower'] ? 1 : 0),
	    			'first_dragon' 	  => ($teamStats['firstDragon'] ? 1 : 0),
	    			'win' 			  => ($teamStats['win']=="Win" ? 1 : 0),
	    			'herald_kill' 	  => $teamStats['riftHeraldKills'],
	    			'baron_kills' 	  => $teamStats['baronKills'],
	    			'tower_kills' 	  => $teamStats['towerKills'],
	    			'dragon_kills' 	  => $teamStats['dragonKills'],
	    			'inhibitor_kills' => $teamStats['inhibitorKills']
	    			]);
    		}

    	}
    }
    public static function isTeam($matchId, $teamId)
    {
    	$count = TeamMatch::where('match_id',$matchId)->where('team_id',$teamId)->count();
    	if($count == 1)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    	
    }
}
