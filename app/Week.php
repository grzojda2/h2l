<?php

namespace H2l;

use Illuminate\Database\Eloquent\Model;
use H2l\User;
use H2l\UserMatch;
use H2l\TeamMatch;

class Week extends Model
{
	protected $fillable = ['user_id','icon_id','division','lp','winrate','kda','deaths','dmg','vision','cs','game_time','last_game_id'];
    public static function storeNewUserGames($leagueId,$user_id)
    {
    	$games = Week::catchUserGames($leagueId);
    	$PlayerStats = Week::makeStatistics($games,$leagueId,$user_id);
    	TeamMatch::makeStatistics($games,$leagueId);
    	Week::Create($PlayerStats);
    }
    public static function catchUserGames($leagueId)
    {
    	$url = "https://eun1.api.riotgames.com/lol/match/v4/matchlists/by-account/".$leagueId."?api_key=RGAPI-99f319a1-5d0c-021c-4dd0-8886de9cbaa0";
    	$games = User::decodeJson($url);
    	$i = 0;
    	$gamesDetails = array();
    	$week = 60*60*24*7*1000;
    	$time = time()*1000;
    	foreach($games['matches'] as $game)
    	{
    		if($i<100)
    		{
    			$url = "https://eun1.api.riotgames.com/lol/match/v4/matches/".$game['gameId']."?api_key=RGAPI-99f319a1-5d0c-021c-4dd0-8886de9cbaa0";
    			$gamesDetails[$i] = User::decodeJson($url);
    			if($time-$gamesDetails[$i]['gameCreation']>$week)
    			{
    				$gamesDetails[$i] == "";
    				$i=100;
    			}
    		}
    		$i++;
    	}
    	return $gamesDetails;
    }
    public static function makeStatistics($games,$id,$user_id)
    {
    	$data = [
    		'user_id' 		=> $user_id,
    		'icon_id' 		=> "",
    		'division'		=> "",
    		'lp'	  		=> 0,
    		'winrate' 		=> 0,
    		'kda'	  		=> 0,
    		'deaths'  		=> 0,
    		'dmg'	  		=> 0,
    		'vision'  		=> 0,
    		'cs'	  		=> 0,
    		'game_time' 	=> 0,
    		'last_game_id'  => 0
    	];
    	$count = count($games);
    	foreach($games as $game)
    	{
    		foreach($game['participantIdentities'] as $pi)
    		{
    			if($pi['player']['accountId']==$id)
    			{
    				$participantId   = $pi['participantId'];
    				$summonerId 	 = $pi['player']['summonerId'];
    				$data['icon_id'] = $pi['player']['profileIcon'];
    			}
    		}
    		$participantById = $game['participants'][$participantId-1];
    		$userStats 		 = $participantById['stats'];

    		UserMatch::makeStatistics($userStats,$participantById['teamId'],$participantById['spell1Id'],$participantById['spell2Id'],$game['gameId'],$participantById['championId'],$user_id);

    		$userRank 		  = Week::getRank($summonerId);
    		$data['division'] = $userRank['tier'];
    		$data['lp'] 	  = $userRank['leaguePoints'];
    		if($userStats['win']==true)
    		{
    			$data['winrate']++;
    		}
    		$data['game_time']  += $game['gameDuration'];
    		$data['kda'] 		+= ($userStats['kills']+$userStats['assists'])/($userStats['deaths']==0 ? 1 : $userStats['deaths']);
    		$data['deaths'] 	+= $userStats['deaths'];
    		$data['dmg'] 		+= $userStats['totalDamageDealt']/($game['gameDuration']/60);
    		$data['vision'] 	+= $userStats['visionScore'];
    		$data['cs'] 		+= $userStats['totalMinionsKilled']/($game['gameDuration']/60);
    	}
    	$data['game_time'] 	  = round(($data['game_time']/60)/$count, 2);
    	$data['winrate'] 	  = round($data['winrate']/$count, 2);
    	$data['kda'] 		  = round($data['kda']/$count, 2);
    	$data['deaths'] 	  = round($data['deaths']/$count, 2);
    	$data['dmg'] 		  = round($data['dmg']/$count, 2);
    	$data['vision'] 	  = round($data['vision']/$count, 2);
    	$data['last_game_id'] = $games[$count-1]['gameId'];

    	return $data;
    }
    public static function getRank($summonerId)
    {
    	$url = "https://eun1.api.riotgames.com/lol/league/v4/positions/by-summoner/".$summonerId."?api_key=RGAPI-99f319a1-5d0c-021c-4dd0-8886de9cbaa0";
    	$ranks = User::decodeJson($url);
    	if(null!=$ranks)
    	{
    		return $ranks[0];
    	}
    	else
    	{
    		return $ranks = ['tier' => "UNRANKED", 'leaguePoints' => 0];
    	}
    }
}
