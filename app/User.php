<?php

namespace H2l;

use Illuminate\Database\Eloquent\Model;
use H2l\Week;

class User extends Model
{
	protected $fillable = ['league_id', 'nick'];
    public static function isset($nick)
    {
    	$isset = User::where('nick',$nick)->count();
    	if($isset<1)
    	{
    		return false;
    	}
    	if($isset == 1)
    	{
    		return true;
    	}
    }
    public static function storeNewUser($nick)
    {
    	$leagueId = User::getLeagueId($nick);
		if(isset($leagueId['accountId']))
		{
    		$data = User::Create([
    		'league_id' => $leagueId['accountId'],
    		'nick' => $nick
    		]);

    		$user_id = $data->id;
    		Week::storeNewUserGames($leagueId['accountId'],$user_id);
    	}
    	else
    	{
    		return $leagueId;
    	}
    	
    }
    public static function getLeagueId($nick)
    {
    	$url = "https://eun1.api.riotgames.com/lol/summoner/v4/summoners/by-name/".$nick."?api_key=RGAPI-99f319a1-5d0c-021c-4dd0-8886de9cbaa0";
		$data = User::decodeJson($url);
		return $data;
    }
    public static function decodeJson($url)
    {
    	$headers = get_headers($url);
		$response =  substr($headers[0], 9, 3);
		if($response != "200")
		{
			return "404";
		}
		else
		{
		    $json=file_get_contents($url);
		    $json_data = json_decode($json, true);
			return $json_data;
		}

    }
    public function weeks()
    {
    	return $this->hasMany(Week::class);
    }
    public static function spell($key)
    {
    	switch ($key):
    	case 21:
    		return "SummonerBarrier";
    		break;
    	case 1:
    		return "SummonerBoost";
    		break;
    	case 14:
    		return "SummonerDot";
    		break;
    	case 3:
    		return "SummonerExhaust";
    		break;
    	case 4:
    		return "SummonerFlash";
    		break;
    	case 6:
    		return "SummonerHaste";
    		break;
    	case 7:
    		return "SummonerHeal";
    		break;
    	case 13:
    		return "SummonerMana";
    		break;
    	case 30:
    		return "SummonerPoroRecall";
    		break;
    	case 31:
    		return "SummonerPoroThrow";
    		break;
    	case 11:
    		return "SummonerSmite";
    		break;
    	case 32:
    		return "SummonerSnowball";
    		break;
    	case 12:
    		return "SummonerTeleport";
    		break;
    	endswitch;
    }
    public function games()
    {
    	return $this->hasMany(UserMatch::class)->get();
    }
}
