<?php

namespace H2l;

use Illuminate\Database\Eloquent\Model;

class UserMatch extends Model
{
	protected $fillable = ['match_id','team_id','spell1_id','spell2_id','champion_id', 'win', 'turret_kills','first_turret','first_inhibitor','first_blood','double_kills','triple_kills','quadra_kills','penta_kills','kills','deaths','assists','cs','user_id'];
    public static function makeStatistics($userStats,$team,$spellId1,$spellId2,$matchId,$champion,$user_id)
    {
    	$data = [
    		'user_id' 			=> $user_id,
    		'match_id' 			=> $matchId,
    		'team_id'  			=> $team,
    		'spell1_id' 		=> $spellId1,
    		'spell2_id' 		=> $spellId2,
    		'champion_id' 		=> $champion,
    		'kills' 			=> $userStats['kills'],
    		'deaths' 			=> $userStats['deaths'],
    		'assists' 			=> $userStats['assists'],
    		'cs' 				=> $userStats['totalMinionsKilled'],
    		'win' 				=> ($userStats['win'] ? 1 : 0),
    		'turret_kills'  	=> $userStats['turretKills'],
    		'first_turret'  	=> ((isset($userStats['firstTowerKill'])&&$userStats['firstTowerKill']) ? 1 : 0),
    		'first_inhibitor' 	=> (((isset($userStats['firstInhibitorKill'])&&$userStats['firstInhibitorKill']) || (isset($userStats['firstInhibitorAssist'])&&$userStats['firstInhibitorAssist'])) ? 1 : 0),
    		'first_blood' 		=> (($userStats['firstBloodKill'] || $userStats['firstBloodAssist']) ? 1 : 0),
    		'double_kills' 		=> $userStats['doubleKills'],
    		'triple_kills' 		=> $userStats['tripleKills'],
    		'quadra_kills' 		=> $userStats['quadraKills'],
    		'penta_kills' 		=> $userStats['pentaKills']
    	];
    	UserMatch::Create($data);
    }
}
