<?php

namespace H2l\Http\Controllers;

use Illuminate\Http\Request;
use H2l\User;

class UsersController extends Controller
{
    public function index()
    {
    	return view('user.welcome');
    }
    public function store()
    {
    	$nick = request('nick');
    	if(!User::isset($nick))
    	{
            if(is_string($data = User::storeNewUser($nick)))
            {

                return redirect('/')->with('status',$data);
            }
    	}
        return redirect('/user?user='.$nick);  
    }
    public function show()
    {
        $nick = request('user');
        $user = User::where('nick',$nick)->first();
        return view('user.show',compact('user'));
    }
    public function allMatches(User $user)
    {
        return view('user.games',compact('user'));
    }
}
