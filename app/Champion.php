<?php

namespace H2l;

use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
	protected $fillable = ['champ_id','name','pick','ban'];
    public static function addBaned($ban)
    {
    	Champion::uploadChampions();
    	$championBan = Champion::where('champ_id',$ban)->first();
    	$banCount = $championBan['ban'];
        Champion::where('champ_id',$ban)->update(['ban' => $banCount+1]);
    }
    public static function uploadChampions()
    {
    	if(Champion::count()<1)
    	{
    		$url = "http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json";
    		$champions = User::decodeJson($url)['data'];
    		foreach($champions as $champion)
    		{
    			Champion::Create([
    				'champ_id' => $champion['key'],
    				'name'	   => $champion['id'],
    				'ban'	   => 0,
    				'pick'	   => 0
    			]);
    		}
    	}
    }
    public static function addPicked($championId)
    {
    	Champion::uploadChampions();
    	$championPick = Champion::where('champ_id',$championId)->first();
    	$pickCount = $championPick['pick'];
        Champion::where('champ_id',$championId)->update(['pick' => $pickCount+1]);
    }
    public static function getChampName($id)
    {
    	$champ = Champion::where('champ_id',$id)->first();
    	return $champ['name'];
    }
}
