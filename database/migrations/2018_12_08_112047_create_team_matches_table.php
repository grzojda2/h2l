<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id');
            $table->integer('team_id');
            $table->integer('kills');
            $table->integer('deaths');
            $table->integer('assists');
            $table->integer('cs');
            $table->integer('first_blood');
            $table->integer('first_baron');
            $table->integer('first_tower');
            $table->integer('first_dragon');
            $table->integer('win');
            $table->integer('herald_kill');
            $table->integer('baron_kills');
            $table->integer('tower_kills');
            $table->integer('dragon_kills');
            $table->integer('inhibitor_kills');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_matches');
    }
}
