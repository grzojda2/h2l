<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('icon_id');
            $table->string('division');
            $table->integer('lp');
            $table->float('winrate');
            $table->float('kda');
            $table->float('deaths');
            $table->integer('dmg'); //dmg/min
            $table->integer('vision');
            $table->float('cs'); //cs/min
            $table->float('game_time');
            $table->integer('last_game_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weeks');
    }
}
