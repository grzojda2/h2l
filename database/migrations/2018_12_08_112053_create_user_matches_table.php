<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id');
            $table->integer('user_id');
            $table->integer('kills');
            $table->integer('deaths');
            $table->integer('assists');
            $table->integer('cs');
            $table->integer('team_id');
            $table->integer('spell1_id');
            $table->integer('spell2_id');
            $table->integer('champion_id');
            $table->integer('win');
            $table->integer('turret_kills');
            $table->integer('first_turret');
            $table->integer('first_inhibitor');
            $table->integer('first_blood');
            $table->integer('double_kills');
            $table->integer('triple_kills');
            $table->integer('quadra_kills');
            $table->integer('penta_kills');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_matches');
    }
}
