<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>How2League</title>
	<link href="https://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://getbootstrap.com/docs/4.1/examples/cover/cover.css">
</head>
<body class="text-center">
	
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
        </div>
      </header>

      <main role="main" class="inner cover">
        @include('layouts.error')
        @yield('content')
      </main>
	@include('layouts.footer')
    </div>

</body>
</html>