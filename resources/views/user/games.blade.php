@extends('layouts.template')
@section('content')
<h1 class="cover-heading">{{ $user->nick }} - All games</h1>
@php($games = $user->games())
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
  color: black;
}
</style>
<table>
  <tr>
    <th>Win/Lose</th>
    <th>Champion</th>
    <th>K/D/A</th>
    <th>Champion Icon</th>
    <th>Cs</th>
  </tr>
  @foreach($games as $game)
    <tr>
      <td>{{ $game->win == 1 ? "Win" : "Lose" }}</td>
      <td>{{ H2l\Champion::getChampName($game->champion_id) }}</td>
      <td>{{ round(($game->kills+$game->assists)/($game->deaths==0 ? 1 : $game->deaths), 2) }}</td>
      <td><img src="http://ddragon.leagueoflegends.com/cdn/8.24.1/img/champion/{{ H2l\Champion::getChampName($game->champion_id) }}.png"></td>
      <td>{{ $game->cs }}</td>

    </tr>
  @endforeach
</table>
@endsection