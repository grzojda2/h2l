@extends('layouts.template')
@section('content')
<h1 class="cover-heading">Welcome</h1>
        <p class="lead">Here You can check Your LoL stats ;)</p>
        <p class="lead">
          <form action="{{ url('/') }}/store">
            {{ csrf_field() }}
          	<div class="row">
          		<div class="col">
	          		<input class="form-control" type="text" name="nick" placeholder="Nick" required>
	          	</div>
	          	<div class="col-2">
	          	<button type="submit" class="btn btn-primary">Submit</button>
	          	</div>
	        </div>
          </form>
        </p>
@endsection