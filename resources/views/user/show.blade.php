@extends('layouts.template')
@section('content')
@php($week = $user->weeks->first())
<h1 class="cover-heading">{{ $user->nick }} - Last week stats</h1>
  <p><a href="{{ url('/') }}/games/{{ $user->id }}">All games</a></p>
  <p><a href="{{ url('/') }}">Check another player</a></p>
        <p class="lead">
        profile icon:
        </p>
        <p class="lead">
          <img src="http://ddragon.leagueoflegends.com/cdn/8.24.1/img/profileicon/{{ $week->icon_id }}.png">
        </p>

        <p class="lead">
          Division:
        </p>
        <p class="lead">
          {{ $week->division }}
        </p>

        <p class="lead">
          Lp:
        </p>
        <p class="lead">
          {{ $week->lp }}
        </p>

        <p class="lead">
          Winrate:
        </p>
        <p class="lead">
          {{ $week->winrate }}%
        </p>

        <p class="lead">
          K/D/A:
        </p>
        <p class="lead">
          {{ $week->kda }}
        </p>

        <p class="lead">
          Deaths:
        </p>
        <p class="lead">
          {{ $week->deaths }}
        </p>

        <p class="lead">
          Damage:
        </p>
        <p class="lead">
          {{ $week->dmg }}dmg/min
        </p>

        <p class="lead">
          Vision points:
        </p>
        <p class="lead">
          {{ $week->vision }}
        </p>

        <p class="lead">
          Killed Creatures/min:
        </p>
        <p class="lead">
          {{ $week->cs }}/min
        </p>

        <p class="lead">
          Game Time:
        </p>
        <p class="lead">
          {{ $week->game_time }}min
        </p>

@endsection